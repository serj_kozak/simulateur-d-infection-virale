import tkinter as tk
import random as rd
from tkinter import Canvas

HEALTHY_P = []
RECOVERED_P = []
SICK_P = []
DEAD_P = []

MAX_RECOVERED = (0, 0)
MAX_SICK = (0, 0)
MAX_DEAD = (0, 0)

STEP_D = 0
STEP_P = 0
POINT_H = (10, 10)
POINT_R = (10, 270)
POINT_S = (10, 270)
POINT_D = (10, 270)


def simulate():
    canvas_graph.delete(tk.ALL)
    canvas_text.delete(tk.ALL)
    peoples = int(man_ent.get())
    days = int(day_ent.get())
    create_data(peoples)
    create_graph(days, peoples)
    simulation(peoples, days)


def create_data(peoples):
    global HEALTHY_P

    for _ in range(peoples):
        people = {"status": False,
                  "health": rd.randint(1, 3),
                  "day_of_sick": 0
                  }
        HEALTHY_P.append(people)


def simulation(peoples, days):
    global HEALTHY_P
    global SICK_P
    global RECOVERED_P
    global DEAD_P

    global MAX_RECOVERED
    global MAX_SICK
    global MAX_DEAD

    people = rd.choice(HEALTHY_P)
    people["status"] = True
    SICK_P.append(people)
    HEALTHY_P.remove(people)
    for day in range(1, days):
        ytd_recovered = len(RECOVERED_P)
        ytd_sick = len(SICK_P)
        ytd_dead = len(DEAD_P)
        for people in SICK_P:
            check_people(people)
            contacts = rd.randint(0, 2)
            if contacts == 0 or len(HEALTHY_P) == 0:
                continue
            elif contacts >= len(HEALTHY_P):
                create_contacts(len(HEALTHY_P))
            else:
                create_contacts(contacts)
        else:
            create_line(day)
        if MAX_RECOVERED[0] < len(RECOVERED_P) - ytd_recovered:
            MAX_RECOVERED = (len(RECOVERED_P) - ytd_recovered, day + 1)
        if MAX_SICK[0] < len(SICK_P) - ytd_sick:
            MAX_SICK = (len(SICK_P) - ytd_sick, day + 1)
        if MAX_DEAD[0] < len(DEAD_P) - ytd_dead:
            MAX_DEAD = (len(DEAD_P) - ytd_dead, day + 1)

    else:
        create_text()
        remove_params()


def create_contacts(contacts: int):
    global HEALTHY_P
    global SICK_P
    for _ in range(contacts):
        people_new = rd.choice(HEALTHY_P)
        people_new["status"] = True
        SICK_P.append(people_new)
        HEALTHY_P.remove(people_new)
        check_people(people_new)


def check_people(people: dict):
    global SICK_P
    global RECOVERED_P
    global DEAD_P
    people["day_of_sick"] += 1
    if people["day_of_sick"] < 3 or people["day_of_sick"] == 4:
        pass
    elif people["day_of_sick"] == 3 and people["health"] == 1:
        DEAD_P.append(people)
        SICK_P.remove(people)
    elif people["day_of_sick"] == 3 and people["health"] == 3:
        RECOVERED_P.append(people)
        SICK_P.remove(people)
    else:
        RECOVERED_P.append(people)
        SICK_P.remove(people)


def create_graph(days, peoples):
    global STEP_D
    global STEP_P

    canvas_graph.create_line(10, 270, 770, 270)
    canvas_graph.create_line(10, 270, 10, 10)
    # line_days = 760
    # line_peoples = 260
    STEP_D = 760 / days
    STEP_P = 260 / peoples


def create_line(day):
    global POINT_H
    global POINT_R
    global POINT_S
    global POINT_D
    new_point_h = (10+(STEP_D * day), 270 - (STEP_P * (len(HEALTHY_P))))
    canvas_graph.create_line((POINT_H + new_point_h), fill='green')
    POINT_H = new_point_h
    new_point_r = (10+(STEP_D * day), 270 - (STEP_P * (len(RECOVERED_P))))
    canvas_graph.create_line(POINT_R + new_point_r, fill='blue')
    POINT_R = new_point_r
    new_point_s = (10+(STEP_D * day), 270 - (STEP_P * (len(SICK_P))))
    canvas_graph.create_line(POINT_S + new_point_s, fill='red')
    POINT_S = new_point_s
    new_point_d = (10+(STEP_D * day), 270 - (STEP_P * (len(DEAD_P))))
    canvas_graph.create_line(POINT_D + new_point_d, fill='black')
    POINT_D = new_point_d


def create_text():
    canvas_text.create_text(200, 20, text=f"Здорових людей: {len(HEALTHY_P)}",
                            justify=tk.CENTER, font=('Arial', 15, 'bold'), fill="green")
    canvas_text.create_text(200, 50, text=f"Одужавших людей: {len(RECOVERED_P)}",
                            justify=tk.CENTER, font=('Arial', 15, 'bold'), fill="blue")
    canvas_text.create_text(200, 80, text=f"Хворих людей: {len(SICK_P)}",
                            justify=tk.CENTER, font=('Arial', 15, 'bold'), fill="red")
    canvas_text.create_text(200, 110, text=f"Померло людей: {len(DEAD_P)}",
                            justify=tk.CENTER, font=('Arial', 15, 'bold'), fill="black")
    canvas_text.create_text(600, 50, text=f"Макс. одужало: {MAX_RECOVERED[0]} за {MAX_RECOVERED[1]} день",
                            justify=tk.CENTER, font=('Arial', 15, 'bold'), fill="blue")
    canvas_text.create_text(600, 80, text=f"Макс. захворіло: {MAX_SICK[0]} за {MAX_SICK[1]} день",
                            justify=tk.CENTER, font=('Arial', 15, 'bold'), fill="red")
    canvas_text.create_text(600, 110, text=f"Макс. померло: {MAX_DEAD[0]} за {MAX_DEAD[1]} день",
                            justify=tk.CENTER, font=('Arial', 15, 'bold'), fill="black")


def remove_params():
    global HEALTHY_P
    global SICK_P
    global RECOVERED_P
    global DEAD_P
    global MAX_RECOVERED
    global MAX_SICK
    global MAX_DEAD
    global STEP_D
    global STEP_P
    global POINT_H
    global POINT_R
    global POINT_S
    global POINT_D

    HEALTHY_P = []
    RECOVERED_P = []
    SICK_P = []
    DEAD_P = []

    MAX_RECOVERED = (0, 0)
    MAX_SICK = (0, 0)
    MAX_DEAD = (0, 0)

    STEP_D = 0
    STEP_P = 0
    POINT_H = (10, 10)
    POINT_R = (10, 270)
    POINT_S = (10, 270)
    POINT_D = (10, 270)


root = tk.Tk()
root.title('Вірусний симулятор')
root.geometry('800x600+350+100')
root.resizable(False, False)

man_lb = tk.Label(root, text="Кількість людей:", font=('Arial', 15, 'bold'))\
    .grid(row=0, column=0, ipadx=10, ipady=5, stick='w')
man_ent = tk.Entry(root, width=10, bd=3, font=('Arial', 15, 'bold'), justify=tk.LEFT)
man_ent.grid(row=0, column=1, padx=10, pady=5, stick='wens')

day_lb = tk.Label(root, text="Кількість днів:", font=('Arial', 15, 'bold'))\
    .grid(row=0, column=2, ipadx=10, ipady=5, stick='w')
day_ent = tk.Entry(root, width=10, bd=3, font=('Arial', 15, 'bold'), justify=tk.LEFT)
day_ent.grid(row=0, column=3, padx=10, pady=5, stick='wens')

btn_simulate = tk.Button(root, width=0, height=0, bg='green', activebackground='red', text='Симуляція',
                         font=('Arial', 15, 'bold'), command=simulate)\
    .grid(row=1, column=0, padx=10, pady=5, columnspan=4, stick='wens')

canvas_graph = Canvas(root, width=780, height=280, bg="#C0C0C0")
canvas_graph.grid(row=2, column=0, padx=10, pady=5, columnspan=4, stick='wens')

canvas_text = Canvas(root, width=780, height=200, bg="#C0C0C0", cursor="pencil")
canvas_text.grid(row=3, column=0, padx=10, pady=5, columnspan=4, stick='wens')


root.grid_columnconfigure(0, minsize=200)
root.grid_columnconfigure(1, minsize=200)
root.grid_columnconfigure(2, minsize=200)
root.grid_columnconfigure(3, minsize=200)

root.grid_rowconfigure(0, minsize=15)
root.grid_rowconfigure(1, minsize=15)
root.grid_rowconfigure(2, minsize=280)
root.grid_rowconfigure(3, minsize=200)

root.mainloop()
